import 'package:flutter/material.dart';
import 'package:flutter_app/widgets/first_feature_screen.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
      ),
      body: GridView.count(
        mainAxisSpacing: 10,
        crossAxisCount: 2,
        children: [
          Column(
            children: const [
              Card(
                color: Colors.grey,
                child: Icon(
                  Icons.account_circle,
                  size: 150,
                  color: Colors.black,
                ),
              ),
              Text('Profile'),
            ],
          ),
          Column(
            children: const [
              Card(
                color: Colors.grey,
                child: Icon(
                  Icons.settings,
                  size: 150,
                  color: Colors.black,
                ),
              ),
              Text('Settings')
            ],
          ),
          Column(
            children: const [
              Card(
                color: Colors.grey,
                child: Icon(
                  Icons.support_agent_sharp,
                  size: 150,
                  color: Colors.black,
                ),
              ),
              Text('Support')
            ],
          ),
          Column(
            children: const [
              Card(
                color: Colors.grey,
                child: Icon(
                  Icons.info_rounded,
                  size: 150,
                  color: Colors.black,
                ),
              ),
              Text('Information')
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const AnotherScreen()));
        },
        child: const Icon(Icons.add_circle),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
